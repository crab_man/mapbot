use std::sync::Arc;

use clap::Parser;
use cli::Cli;
use db::connection;
use error::Error;
use settings::BotSettings;
use tracing_subscriber::{filter::LevelFilter, EnvFilter};

pub mod cli;
pub mod db;
pub mod discord;
pub mod error;
pub mod external;
pub mod maps;
pub mod settings;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Cli::parse();

    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::from_default_env()
                .add_directive(LevelFilter::from_level(args.log_level()).into()),
        )
        .init();

    let settings = BotSettings::load_init(&args.config).await?;
    let pool = Arc::new(connection(&settings.database_uri).await?);

    discord::start(&settings, Arc::clone(&pool)).await;

    Ok(())
}
