use std::path::PathBuf;

use thiserror::Error;
use tokio::fs;

use crate::{external::MapGenConfig, settings::BotSettings};

pub struct MapManager<'a>(pub &'a BotSettings);

pub struct MapPaths {
    pub image: PathBuf,
    pub metadata: PathBuf,
}

#[derive(Debug, Error)]
pub enum MapManagerError {
    #[error("Reqwest error: {0:?}")]
    RequestError(#[from] reqwest::Error),
    #[error("I/O Error while writing map files: {0:?}")]
    Io(#[from] tokio::io::Error),
    #[error("Error while deserailizing map metadata: {0:?}")]
    DeserializeError(#[from] ron::de::SpannedError),
}

impl<'a> MapManager<'a> {
    pub async fn download(
        &self,
        url: &str,
        seed: u32,
        map_settings: &MapGenConfig,
    ) -> Result<MapPaths, MapManagerError> {
        let paths = self.download_image(url, seed).await?;

        fs::write(
            &paths.metadata,
            &ron::ser::to_string(map_settings).unwrap().into_bytes(),
        )
        .await?;

        Ok(paths)
    }

    pub async fn download_image(&self, url: &str, seed: u32) -> Result<MapPaths, MapManagerError> {
        let request = reqwest::get(url).await?.bytes().await?;
        let paths = self.paths_for(seed);
        fs::write(&paths.image, &request).await?;
        Ok(paths)
    }

    pub async fn get_metadata(&self, seed: u32) -> Result<Option<MapGenConfig>, MapManagerError> {
        let paths = self.paths_for(seed);
        let Ok(content) = fs::read_to_string(paths.metadata).await else {
            return Ok(None);
        };

        let content = ron::from_str(&content)?;

        Ok(Some(content))
    }

    pub fn paths_for(&self, seed: u32) -> MapPaths {
        let base_map_path = self.0.map_path(seed);
        MapPaths {
            image: base_map_path.with_extension("png"),
            metadata: base_map_path.with_extension("ron"),
        }
    }
}
