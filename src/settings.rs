use std::path::{Path, PathBuf};

use serde::Deserialize;
use thiserror::Error;
use tokio::fs;
use tracing::instrument;

pub const MAPS_PATH: &str = "maps";

#[derive(Debug, Error)]
pub enum SettingsError {
    #[error("Could not read configuration from '{0:?}': {0:?}")]
    ReadFailed(PathBuf, tokio::io::Error),
    #[error("Failed to parse configuration: {0:?}")]
    ParseFailed(ron::de::SpannedError),
    #[error("I/O error while initilazing data path: {0:?}")]
    DataPathInitIO(tokio::io::Error),
}

#[derive(Debug, Clone, Deserialize)]
pub struct BotSettings {
    pub database_uri: String,
    pub data_path: PathBuf,
    pub discord: DiscordSettings,
}

#[derive(Debug, Clone, Deserialize)]
pub struct DiscordSettings {
    pub bot_token: String,
    pub guild_id: u64,
    pub roles: DiscordRoles,
    pub channels: DiscordChannels,
}

#[derive(Debug, Clone, Deserialize)]
pub struct DiscordRoles {
    pub experienced_traveler: u64,
    pub map_reviewer: u64,
    pub traveler: u64,
}

#[derive(Debug, Clone, Deserialize)]
pub struct DiscordChannels {
    pub catalog: u64,
}

impl BotSettings {
    #[instrument]
    pub async fn load_init(from: &Path) -> Result<Self, SettingsError> {
        let raw = fs::read_to_string(from)
            .await
            .map_err(|error| SettingsError::ReadFailed(from.to_owned(), error))?;

        let settings: BotSettings = ron::from_str(&raw).map_err(SettingsError::ParseFailed)?;

        tokio::fs::create_dir_all(settings.data_path.join(MAPS_PATH))
            .await
            .map_err(SettingsError::DataPathInitIO)?;

        Ok(settings)
    }

    pub fn map_path(&self, seed: u32) -> PathBuf {
        self.data_path.join(MAPS_PATH).join(seed.to_string())
    }
}
