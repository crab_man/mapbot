use std::sync::Arc;

use poise::serenity_prelude::{self as serenity, GuildId};
use thiserror::Error;
use tracing::warn;

use crate::{db::DbPool, error::Error, settings::BotSettings};

mod approve;
mod upload;
mod util;

pub type CommandContext<'a> = poise::Context<'a, BotData, Error>;

#[derive(Debug, Error)]
pub enum DiscordBotError {
    #[error("Error while registering discord commands: {0:?}")]
    RegisterError(serenity::Error),
    #[error("Serenity error: {0:?}")]
    SerenityError(#[from] serenity::Error),
    #[error("Invalid map generation config: {0:?}")]
    InvalidMapGenerationConfig(ron::de::SpannedError),
}

pub struct BotData {
    pool: Arc<DbPool>,
    settings: BotSettings,
}

pub async fn start(settings: &BotSettings, pool: Arc<DbPool>) {
    let settings2 = settings.clone();

    let framework = poise::Framework::<BotData, Error>::builder()
        .options(poise::FrameworkOptions {
            commands: vec![
                upload::upload(),
                upload::silent_upload(),
                upload::biome_seed(),
                approve::approve(),
                util::is_linked(),
                util::metadata(),
            ],
            ..Default::default()
        })
        .setup(move |ctx, _ready, framework| {
            let pool = Arc::clone(&pool);
            let settings = settings2.clone();

            Box::pin(async move {
                poise::builtins::register_in_guild(
                    ctx,
                    &framework.options().commands,
                    GuildId::new(settings.discord.guild_id),
                )
                .await
                .map_err(DiscordBotError::RegisterError)?;
                Ok(BotData { pool, settings })
            })
        })
        .build();

    let client = serenity::ClientBuilder::new(
        &settings.discord.bot_token,
        serenity::GatewayIntents::privileged(),
    )
    .framework(framework)
    .await;

    client.unwrap().start().await.unwrap();
}

async fn role_check(context: &CommandContext<'_>, role_id: u64) -> bool {
    let Some(min_role_position) = context.partial_guild().await.and_then(|guild| {
        guild
            .roles
            .iter()
            .find_map(|(_, role)| Some(role.position).filter(|_| role.id.get() == role_id))
    }) else {
        warn!("Role not found");
        return false;
    };

    !context
        .author_member()
        .await
        .and_then(|member| member.roles(context.cache()))
        .is_some_and(|roles| roles.iter().any(|role| role.position >= min_role_position))
}
