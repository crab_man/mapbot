use std::fmt::Write;

use poise::serenity_prelude::ChannelType;

use crate::{
    db::{actions, models::Map},
    error::Error,
    maps::MapManager,
};

use super::CommandContext;

pub async fn get_thread_map(context: CommandContext<'_>) -> Result<Option<Map>, Error> {
    let Some(channel) = context
        .channel_id()
        .to_channel(context)
        .await?
        .guild()
        .filter(|channel| matches!(channel.kind, ChannelType::PublicThread))
    else {
        context.defer_ephemeral().await?;
        context.reply("Invalid context").await?;
        return Ok(None);
    };

    if let Some(map) =
        actions::map::get_by_thread_id(&channel.id.get().to_string(), context.data().pool.as_ref())
            .await?
    {
        return Ok(Some(map));
    } else {
        Ok(None)
    }
}

#[poise::command(slash_command)]
/// Checks if the current thread is linked to a map
pub async fn is_linked(context: CommandContext<'_>) -> Result<(), Error> {
    if let Some(map) = get_thread_map(context).await? {
        context
            .reply(format!(
                "This thread is linked to a map with seed `{}`",
                map.seed
            ))
            .await?;
    } else {
        context.reply("Thread not linked to a map").await?;
    }

    Ok(())
}

#[poise::command(slash_command)]
pub async fn metadata(context: CommandContext<'_>) -> Result<(), Error> {
    if let Some(map) = get_thread_map(context).await? {
        let metadata = MapManager(&context.data().settings)
            .get_metadata(map.seed)
            .await?;
        let mut meta = metadata.map_or_else(
            || "Map has no metadat".to_string(),
            |meta| {
                format!(
                    "```rust\n{}```",
                    ron::ser::to_string_pretty(&meta, Default::default())
                        .expect("Serializing map metadata failed")
                )
            },
        );

        if let Some(biome_seed) = map.biome_seed {
            write!(&mut meta, "\nBiome seed: `{biome_seed}`").unwrap();
        }

        context.reply(meta).await?;
    } else {
        context.reply("Thread not linked to a map").await?;
    }

    Ok(())
}
