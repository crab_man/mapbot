use poise::{
    serenity_prelude::{
        Attachment, ChannelType, CreateAllowedMentions, CreateAttachment, CreateMessage,
        CreateThread, MessageFlags, RoleId,
    },
    CreateReply,
};
use tracing::{debug, instrument, trace};

use crate::{
    db::{actions, DatabaseError, Transaction},
    discord::{role_check, util::get_thread_map},
    error::Error,
    external::MapGenConfig,
    maps::{MapManager, MapPaths},
};

use super::{CommandContext, DiscordBotError};

#[poise::command(slash_command)]
#[instrument(skip(context, image, metadata))]
pub async fn upload(
    context: CommandContext<'_>,
    #[description = "Image of the map"] image: Attachment,
    #[description = "Map metadata"] metadata: String,
    #[description = "who generated this map"] author: Option<String>,
) -> Result<(), Error> {
    let data = context.data();
    let mut transaction = data
        .pool
        .begin()
        .await
        .map_err(DatabaseError::TransactionBeginError)?;

    if !role_check(&context, data.settings.discord.roles.traveler).await {
        context.defer_ephemeral().await?;
        context.reply("Not enough permissions").await?;
        return Ok(());
    }

    let author = author
        .as_deref()
        .unwrap_or_else(|| context.author().name.as_str());

    let Some((seed, paths, metadata)) =
        upload_inner(context, &mut transaction, image, author, &metadata).await?
    else {
        return Ok(());
    };

    let message = context
        .send(
            CreateReply::default()
                .content(format!("New map created by @{author}\n"))
                .attachment(CreateAttachment::path(paths.image).await?)
                .allowed_mentions(CreateAllowedMentions::default()),
        )
        .await?
        .into_message()
        .await?;

    let thread = message
        .channel_id
        .create_thread_from_message(
            &context,
            message.id,
            CreateThread::new(seed.to_string())
                .invitable(true)
                .kind(ChannelType::PublicThread),
        )
        .await?;

    let message = thread
        .send_message(
            &context,
            CreateMessage::new()
                .content(format!(
                    "```rust\n{}```\n<@&{}>",
                    ron::ser::to_string_pretty(&metadata, Default::default()).unwrap(),
                    data.settings.discord.roles.map_reviewer
                ))
                .flags(MessageFlags::SUPPRESS_NOTIFICATIONS)
                .allowed_mentions(
                    CreateAllowedMentions::new()
                        .roles([RoleId::new(data.settings.discord.roles.map_reviewer)]),
                ),
        )
        .await?;

    message.pin(&context).await?;

    actions::map::set_review_thread(seed, &thread.id.get().to_string(), transaction.as_mut())
        .await?;

    transaction
        .commit()
        .await
        .map_err(DatabaseError::TransactionCommitError)?;

    Ok(())
}

#[poise::command(slash_command)]
#[instrument(skip(context, image))]
pub async fn biome_seed(
    context: CommandContext<'_>,
    #[description = "Image of the map"] image: Attachment,
    #[description = "Biome seed"] biome_seed: u32,
) -> Result<(), Error> {
    let Some(map) = get_thread_map(context).await? else {
        context.defer_ephemeral().await?;
        context.reply("Thread not linked to a map").await?;
        return Ok(());
    };

    let mut transaction = context
        .data()
        .pool
        .begin()
        .await
        .map_err(DatabaseError::TransactionBeginError)?;

    actions::map::set_biome_seed(map.seed, biome_seed, transaction.as_mut()).await?;

    MapManager(&context.data().settings)
        .download_image(&image.url, map.seed)
        .await?;

    transaction
        .commit()
        .await
        .map_err(DatabaseError::TransactionCommitError)?;

    context
        .reply(format!("Biome seed updated to `{}`", biome_seed))
        .await?;

    Ok(())
}

#[poise::command(slash_command)]
#[instrument(skip(context, image, metadata))]
pub async fn silent_upload(
    context: CommandContext<'_>,
    #[description = "Image of the map"] image: Attachment,
    #[description = "Map metadata"] metadata: String,
    #[description = "who generated this map"] author: Option<String>,
    #[description = "Whether the current thread should be saved as origin thread of the map"]
    set_current_thread: bool,
) -> Result<(), Error> {
    let data = context.data();
    let mut transaction = data
        .pool
        .begin()
        .await
        .map_err(DatabaseError::TransactionBeginError)?;

    if !role_check(&context, data.settings.discord.roles.experienced_traveler).await {
        context.defer_ephemeral().await?;
        context.reply("Not enough permissions").await?;
        return Ok(());
    }

    let author = author
        .as_deref()
        .unwrap_or_else(|| context.author().name.as_str());

    let Some((seed, _paths, _metadata)) =
        upload_inner(context, &mut transaction, image, author, &metadata).await?
    else {
        return Ok(());
    };

    if set_current_thread {
        let Some(channel) = context
            .channel_id()
            .to_channel(context)
            .await?
            .guild()
            .filter(|channel| matches!(channel.kind, ChannelType::PublicThread))
        else {
            context.defer_ephemeral().await?;
            context
                .reply("set_current_thread only works in public threads")
                .await?;
            return Ok(());
        };

        if actions::map::get_by_thread_id(&channel.id.get().to_string(), transaction.as_mut())
            .await?
            .is_some()
        {
            context.defer_ephemeral().await?;
            context
                .reply("This thread is already assigned to a map")
                .await?;
            return Ok(());
        }

        actions::map::set_review_thread(seed, &channel.id.get().to_string(), transaction.as_mut())
            .await?;

        context
            .reply(format!(
                "Current thread was linked to seed `{}` by <@{}>",
                seed,
                context.author().id.get(),
            ))
            .await?;
    } else {
        context
            .reply(format!(
                "<@{}> uploaded seed `{}.`",
                context.author().id.get(),
                seed,
            ))
            .await?;
    }

    transaction
        .commit()
        .await
        .map_err(DatabaseError::TransactionCommitError)?;

    Ok(())
}

#[instrument(skip(context, image, author, metadata))]
async fn upload_inner(
    context: CommandContext<'_>,
    transaction: &mut Transaction<'_>,
    image: Attachment,
    author: &str,
    metadata: &str,
) -> Result<Option<(u32, MapPaths, MapGenConfig)>, Error> {
    let data = context.data();
    let discord_id = context.author().id.get().to_string();

    let metadata = ron::from_str::<MapGenConfig>(metadata)
        .map_err(DiscordBotError::InvalidMapGenerationConfig)?;
    let seed = metadata.seed;

    let seed_hunter = if let Some(seed_hunter) =
        actions::seed_hunter::get(&discord_id, transaction.as_mut()).await?
    {
        seed_hunter
    } else {
        debug!(?discord_id, "Inserting new seed hunter");
        actions::seed_hunter::insert(&discord_id, transaction.as_mut()).await?
    };

    trace!("Checking if map already exists");
    // Check if a map with this seed already exists
    if actions::map::get(seed, transaction.as_mut())
        .await?
        .is_some()
    {
        context.defer_ephemeral().await?;
        context.reply("A map with this seed already exists").await?;
        return Ok(None);
    }

    trace!("Inserting new map");
    actions::map::create(seed, seed_hunter.id, author, transaction.as_mut()).await?;

    let paths = MapManager(&data.settings)
        .download(&image.url, seed, &metadata)
        .await?;

    Ok(Some((seed, paths, metadata)))
}
