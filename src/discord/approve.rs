use poise::serenity_prelude::{ChannelId, CreateAttachment, CreateMessage, EditThread};

use crate::{
    db::{actions, DatabaseError},
    discord::role_check,
    error::Error,
    maps::MapManager,
};

use super::CommandContext;

#[poise::command(slash_command)]
pub async fn approve(context: CommandContext<'_>) -> Result<(), Error> {
    let data = context.data();

    if !role_check(&context, data.settings.discord.roles.experienced_traveler).await {
        context.defer_ephemeral().await?;
        context.reply("Not enough permissions").await?;
        return Ok(());
    }

    let mut transaction = data
        .pool
        .begin()
        .await
        .map_err(DatabaseError::TransactionBeginError)?;

    if let Some(map) = actions::map::get_by_thread_id(
        &context.channel_id().get().to_string(),
        transaction.as_mut(),
    )
    .await
    .map_err(Error::DatabaseError)?
    {
        if map.approved.unwrap_or(false) {
            context.reply("Map already approved").await?;
            return Ok(());
        }


        actions::map::approve(map.seed, transaction.as_mut()).await?;
        context
            .reply(format!("Map approved by <@{}>", context.author().id.get()))
            .await?;
        let paths = MapManager(&data.settings).paths_for(map.seed);
        ChannelId::new(data.settings.discord.channels.catalog)
            .send_message(
                context,
                CreateMessage::new()
                    .content(format!(
                        "Map by @{}: <#{}>",
                        map.author,
                        map.review_thread_id.as_deref().unwrap_or("")
                    ))
                    .add_file(CreateAttachment::path(paths.image).await?),
            )
            .await?;

        if let Some(mut thread) = context.channel_id().to_channel(context).await?.guild() {
            thread
                .edit_thread(context, EditThread::new().archived(true))
                .await?;
        }
    } else {
        context.defer_ephemeral().await?;
        context.reply("This is not a map review thread").await?;
    }

    transaction
        .commit()
        .await
        .map_err(DatabaseError::PoolCreateError)?;

    Ok(())
}
