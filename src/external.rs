use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct MapGenConfig {
    pub gen_opts: GenOpts,
    pub seed: u32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct GenOpts {
    pub erosion_quality: f32,
    pub map_kind: MapKind,
    pub scale: f64,
    pub x_lg: u32,
    pub y_lg: u32,
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub enum MapKind {
    Square,
    Circle,
}
