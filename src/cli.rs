use std::path::PathBuf;

use clap::Parser;
use tracing::Level;

#[derive(Debug, Parser)]
pub struct Cli {
    /// Configuration path
    #[arg(short, long)]
    pub config: PathBuf,
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub verbose: u8,
}

impl Cli {
    pub fn log_level(&self) -> Level {
        match self.verbose {
            0 => Level::WARN,
            1 => Level::INFO,
            2 => Level::DEBUG,
            _ => Level::TRACE,
        }
    }
}
