use thiserror::Error;

use crate::{
    db::DatabaseError, discord::DiscordBotError, maps::MapManagerError, settings::SettingsError,
};

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    DatabaseError(#[from] DatabaseError),
    #[error(transparent)]
    ConfigError(#[from] SettingsError),
    #[error(transparent)]
    MapManagerError(#[from] MapManagerError),
    #[error(transparent)]
    Discord(#[from] DiscordBotError),
}

impl From<poise::serenity_prelude::Error> for Error {
    fn from(value: poise::serenity_prelude::Error) -> Self {
        Self::Discord(DiscordBotError::SerenityError(value))
    }
}
