pub mod map {
    use sqlx::{Executor, FromRow, Sqlite};

    use crate::db::{models, DatabaseError};

    pub async fn get(
        seed: u32,
        connection: impl Executor<'_, Database = Sqlite>,
    ) -> Result<Option<models::Map>, DatabaseError> {
        let result = sqlx::query("SELECT * FROM map WHERE seed = ?1")
            .bind(seed)
            .fetch_optional(connection)
            .await
            .map_err(DatabaseError::QueryError)?;

        result
            .map(|row| models::Map::from_row(&row))
            .transpose()
            .map_err(DatabaseError::QueryParseError)
    }

    pub async fn get_by_thread_id(
        thread_id: &str,
        connection: impl Executor<'_, Database = Sqlite>,
    ) -> Result<Option<models::Map>, DatabaseError> {
        let result = sqlx::query("SELECT * FROM map WHERE review_thread_id = ?1")
            .bind(thread_id)
            .fetch_optional(connection)
            .await
            .map_err(DatabaseError::QueryError)?;

        result
            .map(|row| models::Map::from_row(&row))
            .transpose()
            .map_err(DatabaseError::QueryParseError)
    }

    pub async fn create(
        seed: u32,
        creator: i64,
        author: &str,
        connection: impl Executor<'_, Database = Sqlite>,
    ) -> Result<(), DatabaseError> {
        sqlx::query("INSERT INTO map (seed, uploader, author) VALUES (?1, ?2, ?3)")
            .bind(seed)
            .bind(creator)
            .bind(author)
            .execute(connection)
            .await
            .map_err(DatabaseError::QueryError)?;

        Ok(())
    }

    pub async fn set_review_thread(
        seed: u32,
        review_thread: &str,
        connection: impl Executor<'_, Database = Sqlite>,
    ) -> Result<(), DatabaseError> {
        sqlx::query("UPDATE map SET review_thread_id = ?1 WHERE seed = ?2")
            .bind(review_thread)
            .bind(seed)
            .execute(connection)
            .await
            .map_err(DatabaseError::QueryError)?;

        Ok(())
    }

    pub async fn set_biome_seed(
        seed: u32,
        biome_seed: u32,
        connection: impl Executor<'_, Database = Sqlite>,
    ) -> Result<(), DatabaseError> {
        sqlx::query("UPDATE map SET biome_seed = ?1 WHERE seed = ?2")
            .bind(biome_seed)
            .bind(seed)
            .execute(connection)
            .await
            .map_err(DatabaseError::QueryError)?;

        Ok(())
    }

    pub async fn approve(
        seed: u32,
        connection: impl Executor<'_, Database = Sqlite>,
    ) -> Result<(), DatabaseError> {
        sqlx::query("UPDATE map SET approved = true WHERE seed = ?1")
            .bind(seed)
            .execute(connection)
            .await
            .map_err(DatabaseError::QueryError)?;

        Ok(())
    }
}

pub mod seed_hunter {
    use sqlx::{Executor, FromRow, Sqlite};

    use crate::db::{models, DatabaseError};

    pub async fn get(
        discord_id: &str,
        connection: impl Executor<'_, Database = Sqlite>,
    ) -> Result<Option<models::SeedHunter>, DatabaseError> {
        let result = sqlx::query("SELECT * FROM seed_hunter WHERE discord_id = ?1")
            .bind(discord_id)
            .fetch_optional(connection)
            .await
            .map_err(DatabaseError::QueryError)?;

        result
            .map(|row| models::SeedHunter::from_row(&row))
            .transpose()
            .map_err(DatabaseError::QueryParseError)
    }

    pub async fn insert(
        discord_id: &str,
        connection: impl Executor<'_, Database = Sqlite>,
    ) -> Result<models::SeedHunter, DatabaseError> {
        let result = sqlx::query("INSERT INTO seed_hunter (discord_id) VALUES (?1) RETURNING *")
            .bind(discord_id)
            .fetch_one(connection)
            .await
            .map_err(DatabaseError::QueryError)?;

        models::SeedHunter::from_row(&result).map_err(DatabaseError::QueryParseError)
    }
}
