use sqlx::{Pool, Sqlite};
use thiserror::Error;
use tracing::instrument;

pub mod actions;
pub mod models;

pub type DbPool = Pool<Sqlite>;
pub type Transaction<'a> = sqlx::Transaction<'a, Sqlite>;

#[derive(Debug, Error)]
pub enum DatabaseError {
    #[error("Error while creating database pool: {0:?}")]
    PoolCreateError(sqlx::Error),
    #[error("Failed to commit transaction: {0:?}")]
    TransactionCommitError(sqlx::Error),
    #[error("Could not begin transaction: {0:?}")]
    TransactionBeginError(sqlx::Error),
    #[error("Database query failed: {0:?}")]
    QueryError(sqlx::Error),
    #[error("Could not parse database query: {0:?}")]
    QueryParseError(sqlx::Error),
}

#[instrument("create_db_connection")]
pub async fn connection(uri: &str) -> Result<DbPool, DatabaseError> {
    let pool = DbPool::connect(uri)
        .await
        .map_err(DatabaseError::PoolCreateError)?;

    sqlx::migrate!()
        .run(&pool)
        .await
        .expect("Database migrations failed");

    Ok(pool)
}
