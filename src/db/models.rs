use sqlx::FromRow;

#[derive(FromRow)]
pub struct Map {
    pub seed: u32,
    pub uploader: Option<i64>,
    pub author: String,
    pub review_thread_id: Option<String>,
    pub approved: Option<bool>,
    pub biome_seed: Option<u32>,
}

#[derive(FromRow)]
pub struct SeedHunter {
    pub id: i64,
    pub discord_id: String,
}
