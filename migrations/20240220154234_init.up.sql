-- Add up migration script here

CREATE TABLE seed_hunter (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    discord_id TEXT UNIQUE NOT NULL
);

CREATE TABLE map (
    seed INTEGER PRIMARY KEY,
    uploader INTEGER,
    author TEXT NOT NULL,
    review_thread_id TEXT,
    approved BOOLEAN NOT NULL DEFAULT false,
    FOREIGN KEY(uploader) REFERENCES seed_hunter(id) ON DELETE SET NULL
);
