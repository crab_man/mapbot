FROM debian:stable
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y gcc make curl git
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
WORKDIR /opt/build
COPY Cargo.toml Cargo.lock .
COPY src src
COPY migrations migrations
RUN /bin/bash -c "source $HOME/.cargo/env && cargo build --release"

FROM debian:stable-slim

WORKDIR /opt/mapbot
RUN apt-get update && apt-get install -y ca-certificates
COPY --from=0 /opt/build/target/release/mapbot .

ENV RUST_BACKTRACE=full
CMD ./mapbot --config config.ron -v

